import argparse
from typing import TextIO, List


class Rule:
    def __init__(self, pair, insertion):
        self.pair = pair
        self.insertion = insertion

    def __repr__(self):
        return f'{self.pair} -> {self.insertion}'


class Polymer:
    def __init__(self, template):
        self.pairs = {}
        self.polymers = {}
        for index in range(len(template) - 1):
            current_letter = template[index]
            next_letter = template[index + 1]
            pair = f'{current_letter}{next_letter}'
            if pair not in self.pairs:
                self.pairs[pair] = 1
            else:
                self.pairs[pair] += 1
            if current_letter not in self.polymers:
                self.polymers[current_letter] = 1
            else:
                self.polymers[current_letter] += 1
        last_letter = template[-1]
        if last_letter not in self.polymers:
            self.polymers[last_letter] = 1
        else:
            self.polymers[last_letter] += 1

    def apply_rules(self, rules: List[Rule]):
        pair_deltas = {}
        polymer_deltas = {}
        for rule in rules:
            if rule.pair not in self.pairs:
                continue

            count = self.pairs[rule.pair]

            if rule.insertion not in polymer_deltas:
                polymer_deltas[rule.insertion] = 0
            polymer_deltas[rule.insertion] += count

            first = rule.pair[0]
            second = rule.pair[1]
            new_pair_1 = f'{first}{rule.insertion}'
            new_pair_2 = f'{rule.insertion}{second}'

            if new_pair_1 not in pair_deltas:
                pair_deltas[new_pair_1] = 0
            pair_deltas[new_pair_1] += count
            if new_pair_2 not in pair_deltas:
                pair_deltas[new_pair_2] = 0
            pair_deltas[new_pair_2] += count

            if rule.pair not in pair_deltas:
                pair_deltas[rule.pair] = 0
            pair_deltas[rule.pair] -= count

        for pair in pair_deltas:
            if pair in self.pairs:
                self.pairs[pair] += pair_deltas[pair]
            else:
                self.pairs[pair] = pair_deltas[pair]

        for polymer in polymer_deltas:
            if polymer in self.polymers:
                self.polymers[polymer] += polymer_deltas[polymer]
            else:
                self.polymers[polymer] = polymer_deltas[polymer]

    def most_least_difference(self):
        most = max(self.polymers.values())
        least = min(self.polymers.values())
        return most - least


def build_and_simulate(input_file: TextIO, steps):
    state = 'template'
    template = ''
    rules = []

    for line in input_file.readlines():
        line = line.strip()
        if state == 'template':
            template = line
            state = 'rules'
        elif state == 'rules':
            if line == '':
                continue
            pair, insertion = line.split(' -> ')
            rules.append(Rule(pair, insertion))

    polymer = Polymer(template)

    for _ in range(steps):
        polymer.apply_rules(rules)
    return polymer.most_least_difference()


def part_1(input_file: TextIO):
    return build_and_simulate(input_file, 10)


def part_2(input_file: TextIO):
    return build_and_simulate(input_file, 40)


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 14')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
